require_relative 'lib/no_s_d_k/version'

Gem::Specification.new do |spec|
  spec.name          = 'no_s_d_k'
  spec.version       = NoSDK::VERSION
  spec.authors       = ['Nuri Yuri']

  spec.summary       = 'A really badly documented SDK'
  spec.description   = "A really badly documented SDK born from the fact that SDK user doesn't read documentation so why bother writing it ?"
  spec.homepage      = 'https://gitlab.com/nosdk/nosdk'
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  # spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/nosdk/nosdk'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/nosdk/nosdk/-/blob/master/changelog.md'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'bin'
  spec.executables   = [] # spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
end
