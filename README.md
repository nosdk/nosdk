# NoSDK

## Qu'est-ce ?

NoSDK est né du constat que les gens du 21ème siècle sont des gros assistés. On constate que certains savent à peine se servir d'un ordinateur et viennent essayer d'utiliser des SDK. Cette situation est relativement embêtante parce que les mainteneurs de SDK se retrouvent face à une horde de personne qui n'est pas capable de lire la documentation du dit SDK, ils se retrouve à faire tout à la place des utilisateurs du SDK alors que le SDK est censé déjà faciliter cette tâche.

De ce fait, NoSDK sera un SDK différent des autres : Il n'aura pas de documentation claire, seulement des brides si c'est jugé nécessaire.

## Fonctionnement de NoSDK

NoSDK se constituera de composants versionnés qui permettront diverses tâches selon les paramètres donnés aux composants. Chaque composants créera son propre module dans le module `NoSDK` et l'application pourra se servir de ce module comme le module en question le permet.

## Exemple

Prennons un module de gestion des arguments et lançons l'application de la façon suivante : 
```sh
./app.rb test.txt -CDirectory --flag --param_explicite="Test" --numerique=123 --avec_espace 33
```

Et supposons que le fichier `app.rb` contient ceci :

```ruby
require 'no_s_d_k'

NoSDK.setup(component_path: File.expand_path('nosdk'))

no = false
yes = true

NoSDK.load(
  component_name: 'ARGV',
  component_conf: {
    application_name: 'App',
    application_description: 'App that does nothing',
    positional_argument_list: [:filename],
    optional_argument_list: [],
    has_infinite_arguments: no,
    kwargments: {
      param_explicite: {
        required: yes
      },
      numerique: {
        value_if_absent: nil,
        cast_proc: -> (value) { value.to_i }
      },
      avec_espace: {
        value_if_absent: nil,
        cast_proc: -> (value) { value.to_i }
      },
      C: {
        value_if_absent: nil,
        is_single_dash: yes,
      }
    },
    flags: {
      flag: {
        value_if_set: true,
        value_otherwise: false
      }
    },
  }
)

p NoSDK::ARGV.all
# {
#   filename: 'test.txt',
#   kwargments: {
#     param_explicite: 'Test',
#     numerique: 123,
#     avec_espace: 33,
#     C: 'Directory'
#   },
#   flags: {
#     flag: true
#   }
# }
```

## Why is this page in an [incomprehensible dead language](https://www.youtube.com/watch?v=pwODwwgE6rA) ?

We stated that NoSDK doesn't have a clear documentation.
