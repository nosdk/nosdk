require 'no_s_d_k/version'

module NoSDK
  class Error < StandardError; end

  @component_conf = {}

  class << self
    # @return [String, nil]
    attr_reader :component_path
    # @return [Hash{String => Hash}]
    attr_reader :component_conf

    # Setup NoSDK
    # @param component_path [String, nil]
    def setup(component_path: nil)
      @component_path = component_path
    end

    # Load a component
    # @param component_name [String]
    # @param component_conf [Hash]
    def load(component_name:, component_conf: {})
      return if self.component_conf[component_name]

      self.component_conf[component_name] = component_conf
      component_path = self.component_path ? File.join(self.component_path, component_name) : component_name
      yarb_componant = "#{component_path}.yarb"
      if File.exist?(yarb_componant)
        RubyVM::InstructionSequence.load_from_binary(File.binread(yarb_componant)).eval
      else
        require File.join(component_path, 'no_sdk_integration.rb')
      end
    end
  end
end
